﻿   using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyPatrol : MonoBehaviour {


    public float moveSpeed;
    public bool moveRigth;

    public Transform wallCheck;
    public float wallCheckRadius;
    public LayerMask whatIsWall;
    private bool hittingWall;

    PlayerManager ma;

    //private bool atEdge;
    //public Transform edgeCheck;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {

        // moving the AI rigth if theres a wall and left and right if it reach the edge
        hittingWall = Physics2D.OverlapCircle(wallCheck.position, wallCheckRadius, whatIsWall);
        //atEdge = Physics2D.OverlapCircle(edgeCheck.position, wallCheckRadius, whatIsWall);
       if (hittingWall )
        
          moveRigth = !moveRigth;
        

        if (moveRigth)
        {
            //animation
            transform.localScale = new Vector3(-1f, 1f, 1f);
            GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            //animation
            transform.localScale = new Vector3(1f, 1f, 1f);
            GetComponent<Rigidbody2D>().velocity = new Vector2(-moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
	}
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("weapon"))
        {           
            ma.count = ma.count + 1;
            ma.SetCountText();
        }

    }

   
}
