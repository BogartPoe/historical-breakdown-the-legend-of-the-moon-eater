﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainManuControlScript : MonoBehaviour {

	public Button level02Button, level03Button,level04Button,level05Button;
	public GameObject lvl11,lvl12;
	public GameObject lvl21,lvl22;
	public GameObject lvl31,lvl32;

	public GameObject lvl41,lvl42,lvl43;
	int levelPassed;

	// Use this for initialization
	void Start () {
		levelPassed = PlayerPrefs.GetInt ("LevelPassed");
		level02Button.interactable = false;
		level03Button.interactable = false;
		level04Button.interactable = false;
		level05Button.interactable = false;

		switch (levelPassed) {
		case 1:
			level02Button.interactable = true;
			lvl11.SetActive(true);
			lvl12.SetActive(true);
			break;
		case 2:
			level02Button.interactable = true;
			level03Button.interactable = true;
			lvl11.SetActive(true);
			lvl12.SetActive(true);
			lvl21.SetActive(true);
			lvl22.SetActive(true);
			break;
		case 3:
			level02Button.interactable = true;
			level03Button.interactable = true;
			level04Button.interactable = true;
			lvl11.SetActive(true);
			lvl12.SetActive(true);
			lvl21.SetActive(true);
			lvl22.SetActive(true);
			lvl31.SetActive(true);
			lvl32.SetActive(true);
			break;
		case 4:
			level02Button.interactable = true;
			level03Button.interactable = true;
			level04Button.interactable = true;
			level05Button.interactable = true;
			lvl11.SetActive(true);
			lvl12.SetActive(true);
			lvl21.SetActive(true);
			lvl22.SetActive(true);
			lvl31.SetActive(true);
			lvl32.SetActive(true);
			lvl41.SetActive(true);
			lvl42.SetActive(true);
			lvl43.SetActive(true);
			break;
		}
	}
	
	public void levelToLoad (int level)
	{
		SceneManager.LoadScene (level);
	}

	public void resetPlayerPrefs()
	{
		level02Button.interactable = false;
		level03Button.interactable = false;
		level04Button.interactable = false;
		level05Button.interactable = false;
			lvl11.SetActive(false);
			lvl12.SetActive(false);
			lvl21.SetActive(false);
			lvl22.SetActive(false);
			lvl31.SetActive(false);
			lvl32.SetActive(false);
			lvl41.SetActive(false);
			lvl42.SetActive(false);
			lvl43.SetActive(false);
		PlayerPrefs.DeleteAll ();
	}
}
