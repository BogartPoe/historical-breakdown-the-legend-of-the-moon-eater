﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour {
    public GameObject  left, rigth, jump, attack, mainbutton,pauseb,resume;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void pause()
    {
        pauseb.SetActive(false);
        left.SetActive(false);
        rigth.SetActive(false);
        jump.SetActive(false);
        attack.SetActive(false);
        mainbutton.SetActive(true);
        resume.SetActive(true);
        Time.timeScale = 0;
    }
    public void resume1()
    {
        pauseb.SetActive(true);
        left.SetActive(true);
        rigth.SetActive(true);
        jump.SetActive(true);
        attack.SetActive(true);
        mainbutton.SetActive(false);
        resume.SetActive(false);
        Time.timeScale = 1;
    }
}
