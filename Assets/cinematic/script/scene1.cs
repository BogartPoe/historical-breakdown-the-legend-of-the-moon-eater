﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scene1 : MonoBehaviour {

    public GameObject textBox,pepe,hermit,maria,kampong;

    public GameObject main,tanda,babae,kupal;
    public Text atext;

    public int currentLine;
    public int endLine;

    public PlayerController player;

    public TextAsset textFile;
    public string[] textLines;
    // Use this for initialization
    void Start()
    {
        if (textFile != null)
        {
            textLines = (textFile.text.Split('\n'));
        }
        if (endLine == 0)
        {
            endLine = textLines.Length - 1;
        }
        
    }
    void Update()
    {
        atext.text = textLines[currentLine];
    }

    public void plusUltra()
    {

       
            currentLine += 1;
    

         if (currentLine == 1)
        {
            pepe.SetActive(true);
            hermit.SetActive(false);
            maria.SetActive(false);
            main.SetActive(true);
        }
         if (currentLine == 2)
        {
            pepe.SetActive(false);
            hermit.SetActive(false);
            maria.SetActive(false);
        }
        if (currentLine == 3)
        {
            pepe.SetActive(false);
            hermit.SetActive(true);
            maria.SetActive(false);
            tanda.SetActive(true);
        }
		if (currentLine == 4)
        {
            pepe.SetActive(true);
            hermit.SetActive(false);
            maria.SetActive(false);
        }
		if (currentLine == 5)
        {
            pepe.SetActive(false);
            hermit.SetActive(true);
            maria.SetActive(false);
        }if (currentLine == 6)
        {
            pepe.SetActive(true);
            hermit.SetActive(false);
            maria.SetActive(false);
        }
		if (currentLine == 7)
        {
            pepe.SetActive(false);
            hermit.SetActive(true);
            maria.SetActive(false);
        }
		if (currentLine == 8)
        {
            pepe.SetActive(true);
            hermit.SetActive(false);
            maria.SetActive(false);
        }
		if (currentLine == 9)
        {
            pepe.SetActive(false);
            hermit.SetActive(true);
            maria.SetActive(false);
        }if (currentLine == 10)
        {
            pepe.SetActive(true);
            hermit.SetActive(false);
            maria.SetActive(false);
        }
		if (currentLine == 11)
        {
            pepe.SetActive(false);
            hermit.SetActive(true);
            maria.SetActive(false);
        }
		if (currentLine == 12)
        {
            pepe.SetActive(false);
            hermit.SetActive(false);
            maria.SetActive(false);
        }
		if (currentLine == 13)
        {
            pepe.SetActive(false);
            hermit.SetActive(false);
            maria.SetActive(true);
            babae.SetActive(true);
        }
		if (currentLine == 14)
        {
            pepe.SetActive(true);
            hermit.SetActive(false);
            maria.SetActive(false);
        }
		if (currentLine == 15)
        {
            pepe.SetActive(false);
            hermit.SetActive(true);
            maria.SetActive(false);
        }
		if (currentLine == 16)
        {
            pepe.SetActive(false);
            hermit.SetActive(false);
            maria.SetActive(false);
        }
		if (currentLine == 17)
        {
            pepe.SetActive(false);
            hermit.SetActive(false);
            maria.SetActive(false);
            kampong.SetActive(true);
            kupal.SetActive(true);
        }
		if (currentLine == 18)
        {
            pepe.SetActive(false);
            hermit.SetActive(false);
            maria.SetActive(false);
            kampong.SetActive(false);
			
        }
		if (currentLine == 19)
        {
            pepe.SetActive(false);
            hermit.SetActive(false);
            maria.SetActive(true);
            
        }
		if (currentLine == 20)
        {
            pepe.SetActive(false);
            hermit.SetActive(true);
            maria.SetActive(false);
        }
		if (currentLine == 21)
        {
            pepe.SetActive(true);
            hermit.SetActive(false);
             kupal.SetActive(false);
             babae.SetActive(false);
           
        }
		if (currentLine == 22)
        {
            pepe.SetActive(false);
            hermit.SetActive(true);
            
        }
		if (currentLine == 23)
        {
            pepe.SetActive(true);
            hermit.SetActive(false);
           
        }
		if (currentLine == 24)
        {
            pepe.SetActive(false);
            hermit.SetActive(true);
            
        }
		if (currentLine == 25)
        {
            pepe.SetActive(false);
            hermit.SetActive(true);
           
        }
		if (currentLine == 26)
        {
            pepe.SetActive(false);
            hermit.SetActive(true);
            
        }
		
		
		
        if (currentLine > endLine)
        {
            textBox.SetActive(false);
            hermit.SetActive(false);
            main.SetActive(false);
            tanda.SetActive(false);
            atext.text = "";
             Application.LoadLevel("finalLevel1");
           
        }
    }
}
