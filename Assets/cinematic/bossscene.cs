﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bossscene : MonoBehaviour {


    public GameObject textBox,pepe,hermit,maria,kampong;
    public Text atext;

    public int currentLine;
    public int endLine;

    public PlayerController player;

    public TextAsset textFile;
    public string[] textLines;
    // Use this for initialization
    void Start()
    {
        if (textFile != null)
        {
            textLines = (textFile.text.Split('\n'));
        }
        if (endLine == 0)
        {
            endLine = textLines.Length - 1;
        }
        
    }
    void Update()
    {
        atext.text = textLines[currentLine];
    }

    public void plusUltra()
    {

       
            currentLine += 1;
    

         if (currentLine == 0)
        {
            pepe.SetActive(true);
            hermit.SetActive(false);
            maria.SetActive(false);
        }
         if (currentLine == 1)
        {
            pepe.SetActive(false);
            hermit.SetActive(false);
            maria.SetActive(false);
        }
        if (currentLine == 2)
        {
            pepe.SetActive(false);
            kampong.SetActive(true);
            maria.SetActive(false);
        }
		if (currentLine == 3)
        {
            pepe.SetActive(true);
            kampong.SetActive(false);
            maria.SetActive(false);
        }
		if (currentLine == 4)
        {
            pepe.SetActive(false);
            kampong.SetActive(true);
            maria.SetActive(false);
        }if (currentLine == 5)
        {
            pepe.SetActive(false);
			hermit.SetActive(true);
            kampong.SetActive(false);
        }
		if (currentLine == 6)
        {
            pepe.SetActive(true);
            hermit.SetActive(false);
            maria.SetActive(false);
        }
		if (currentLine == 7)
        {
            pepe.SetActive(false);
            hermit.SetActive(false);
            maria.SetActive(false);
        }
		if (currentLine == 9)
        {
            pepe.SetActive(false);
            hermit.SetActive(true);
            maria.SetActive(false);
        }if (currentLine == 10)
        {
            pepe.SetActive(true);
            hermit.SetActive(false);
            maria.SetActive(false);
        }
		if (currentLine == 11)
        {
            pepe.SetActive(false);
            hermit.SetActive(true);
            maria.SetActive(false);
        }
		if (currentLine == 12)
        {
            pepe.SetActive(false);
            hermit.SetActive(false);
            maria.SetActive(false);
        }
	
		
		
		
        if (currentLine > endLine)
        {
            textBox.SetActive(false);
            hermit.SetActive(false);
            atext.text = "";
             Application.LoadLevel("FinalMainMenu");
           
        }
    }
}
