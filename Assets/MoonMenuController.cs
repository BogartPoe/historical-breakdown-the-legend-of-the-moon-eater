﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoonMenuController : MonoBehaviour {

	public Button moon1,moon2,moon3,moon4,moon5,moon6,moon7;
	
	public int levelPassed;

	// Use this for initialization
	void Start () {
		levelPassed = PlayerPrefs.GetInt ("LevelPassed");
		moon1.interactable = false;
		moon2.interactable = false;
		moon3.interactable = false;
		moon4.interactable = false;
		moon5.interactable = false;
		moon6.interactable = false;
		moon7.interactable = false;

		switch (levelPassed) {
		case 1:
			moon1.interactable = true;
			break;
		case 2:
			moon1.interactable = true;
			moon2.interactable = true;
			break;
		case 3:
			moon1.interactable = true;
			moon2.interactable = true;
			moon3.interactable = true;
			break;
		case 4:
			moon1.interactable = true;
			moon2.interactable = true;
			moon3.interactable = true;
			moon4.interactable = true;
			break;
		case 5:
			moon1.interactable = true;
			moon2.interactable = true;
			moon3.interactable = true;
			moon4.interactable = true;
			moon5.interactable = true;
			break;
		case 6:
			moon1.interactable = true;
			moon2.interactable = true;
			moon3.interactable = true;
			moon4.interactable = true;
			moon5.interactable = true;
			moon6.interactable = true;
			break;
		case 7:
			moon1.interactable = true;
			moon2.interactable = true;
			moon3.interactable = true;
			moon4.interactable = true;
			moon5.interactable = true;
			moon6.interactable = true;
			moon7.interactable = true;
			break;
		}
	}
	
	// public void levelToLoad (int level)
	// {
	// 	SceneManager.LoadScene (level);
	// }

	public void resetPlayerPrefs()
	{
		moon1.interactable = false;
			moon2.interactable = false;
			moon3.interactable = false;
			moon4.interactable = false;
			moon5.interactable = false;
			moon6.interactable = false;
			moon7.interactable = false;
		PlayerPrefs.DeleteAll ();
	}
}
